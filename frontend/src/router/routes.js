

export const routes = [
    {
        path: "/",
        name: "home",
        component: () =>
            import("@/components/Home")
    },
    {
        path: "/travel",
        name: "travel",
        component: () =>
            import("@/components/pages/travel")
    },
    {
        path: "/about",
        name: "about",
        component: () =>
            import("@/components/pages/about")
    },
    {
        path: "/webDev",
        name: "webDev",
        component: () =>
            import("@/components/pages/webDev")
    },
    {
        path: "/film",
        name: "film",
        component: () =>
            import("@/components/pages/film")
    },
    {
        path: "/podcast",
        name: "podcast",
        component: () =>
            import("@/components/pages/podcast")
    },
]