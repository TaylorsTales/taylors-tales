import {api} from "@/api/webDev";

export const state = {
    email: null,
};

export const getters = {
    listEmail(){
        return state.email;
    },
};

export const mutations = {
    SET_EMAIL(state, payload) {
        state.email = payload;
    },
};

export const actions = {
    async email({ commit }, data){
        console.log(data)
        const response = await api.email(data);
        if(response){
            const payload = response.data;
            commit("SET_EMAIL", payload);
            return payload;
        }
    },
};